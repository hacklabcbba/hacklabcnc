EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:motordrv
LIBS:cnc_motor_driver_shield-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 5450 2550 2    60   Input ~ 0
MT1_DIR
Text GLabel 5450 2450 2    60   Input ~ 0
MT1_STEP
$Comp
L +5V #PWR01
U 1 1 589EB112
P 5350 2150
F 0 "#PWR01" H 5350 2000 50  0001 C CNN
F 1 "+5V" V 5350 2350 50  0000 C CNN
F 2 "" H 5350 2150 50  0000 C CNN
F 3 "" H 5350 2150 50  0000 C CNN
	1    5350 2150
	0    1    1    0   
$EndComp
Text GLabel 5450 2350 2    60   Input ~ 0
MT1_EN
Text GLabel 4550 2350 0    60   Input ~ 0
IN1_1A
Text GLabel 4550 2450 0    60   Input ~ 0
IN1_1B
Text GLabel 4550 2150 0    60   Input ~ 0
IN1_2A
Text GLabel 4550 2250 0    60   Input ~ 0
IN1_2B
$Comp
L CONN_02X05 P1
U 1 1 58A7DCD2
P 5000 2350
F 0 "P1" H 5000 2650 50  0000 C CNN
F 1 "CONN_02X05" H 5000 2050 50  0000 C CNN
F 2 "Connect:IDC_Header_Straight_10pins" H 5000 1150 50  0001 C CNN
F 3 "" H 5000 1150 50  0000 C CNN
	1    5000 2350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 58A5B4C0
P 4650 2650
F 0 "#PWR02" H 4650 2400 50  0001 C CNN
F 1 "GND" H 4650 2500 50  0000 C CNN
F 2 "" H 4650 2650 50  0000 C CNN
F 3 "" H 4650 2650 50  0000 C CNN
	1    4650 2650
	1    0    0    -1  
$EndComp
Text GLabel 5450 3550 2    60   Input ~ 0
MT2_DIR
Text GLabel 5450 3450 2    60   Input ~ 0
MT2_STEP
$Comp
L +5V #PWR03
U 1 1 58A76032
P 5350 3150
F 0 "#PWR03" H 5350 3000 50  0001 C CNN
F 1 "+5V" V 5350 3350 50  0000 C CNN
F 2 "" H 5350 3150 50  0000 C CNN
F 3 "" H 5350 3150 50  0000 C CNN
	1    5350 3150
	0    1    1    0   
$EndComp
Text GLabel 5450 3350 2    60   Input ~ 0
MT2_EN
Text GLabel 4550 3350 0    60   Input ~ 0
IN2_1A
Text GLabel 4550 3450 0    60   Input ~ 0
IN2_1B
Text GLabel 4550 3150 0    60   Input ~ 0
IN2_2A
Text GLabel 4550 3250 0    60   Input ~ 0
IN2_2B
$Comp
L CONN_02X05 P2
U 1 1 58A76045
P 5000 3350
F 0 "P2" H 5000 3650 50  0000 C CNN
F 1 "CONN_02X05" H 5000 3050 50  0000 C CNN
F 2 "Connect:IDC_Header_Straight_10pins" H 5000 2150 50  0001 C CNN
F 3 "" H 5000 2150 50  0000 C CNN
	1    5000 3350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 58A7604B
P 4650 3650
F 0 "#PWR04" H 4650 3400 50  0001 C CNN
F 1 "GND" H 4650 3500 50  0000 C CNN
F 2 "" H 4650 3650 50  0000 C CNN
F 3 "" H 4650 3650 50  0000 C CNN
	1    4650 3650
	1    0    0    -1  
$EndComp
Text GLabel 7350 2750 2    60   Input ~ 0
IN1_1A
Text GLabel 7350 2850 2    60   Input ~ 0
IN1_1B
Text GLabel 7350 2950 2    60   Input ~ 0
MT1_DIR
Text GLabel 7350 3050 2    60   Input ~ 0
MT1_STEP
Text GLabel 7350 3150 2    60   Input ~ 0
MT1_EN
Text GLabel 7350 4000 2    60   Input ~ 0
MT2_DIR
Text GLabel 7350 3900 2    60   Input ~ 0
MT2_STEP
$Comp
L R R1
U 1 1 58A76AAF
P 7100 2750
F 0 "R1" V 7150 2600 50  0000 C CNN
F 1 "R" V 7100 2750 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 2750 50  0001 C CNN
F 3 "" H 7100 2750 50  0000 C CNN
	1    7100 2750
	0    1    -1   0   
$EndComp
$Comp
L R R2
U 1 1 58A76BA2
P 7100 2850
F 0 "R2" V 7150 2700 50  0000 C CNN
F 1 "R" V 7100 2850 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 2850 50  0001 C CNN
F 3 "" H 7100 2850 50  0000 C CNN
	1    7100 2850
	0    1    -1   0   
$EndComp
$Comp
L R R3
U 1 1 58A76BBE
P 7100 2950
F 0 "R3" V 7150 2800 50  0000 C CNN
F 1 "R" V 7100 2950 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 2950 50  0001 C CNN
F 3 "" H 7100 2950 50  0000 C CNN
	1    7100 2950
	0    1    -1   0   
$EndComp
$Comp
L R R4
U 1 1 58A76C15
P 7100 3050
F 0 "R4" V 7150 2900 50  0000 C CNN
F 1 "R" V 7100 3050 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 3050 50  0001 C CNN
F 3 "" H 7100 3050 50  0000 C CNN
	1    7100 3050
	0    1    -1   0   
$EndComp
$Comp
L R R5
U 1 1 58A76C41
P 7100 3150
F 0 "R5" V 7150 3000 50  0000 C CNN
F 1 "R" V 7100 3150 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 3150 50  0001 C CNN
F 3 "" H 7100 3150 50  0000 C CNN
	1    7100 3150
	0    1    -1   0   
$EndComp
$Comp
L R R6
U 1 1 58A76F7C
P 7100 3600
F 0 "R6" V 7150 3450 50  0000 C CNN
F 1 "R" V 7100 3600 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 3600 50  0001 C CNN
F 3 "" H 7100 3600 50  0000 C CNN
	1    7100 3600
	0    1    -1   0   
$EndComp
$Comp
L R R7
U 1 1 58A76F82
P 7100 3700
F 0 "R7" V 7150 3550 50  0000 C CNN
F 1 "R" V 7100 3700 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 3700 50  0001 C CNN
F 3 "" H 7100 3700 50  0000 C CNN
	1    7100 3700
	0    1    -1   0   
$EndComp
$Comp
L R R8
U 1 1 58A76F88
P 7100 3800
F 0 "R8" V 7150 3650 50  0000 C CNN
F 1 "R" V 7100 3800 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 3800 50  0001 C CNN
F 3 "" H 7100 3800 50  0000 C CNN
	1    7100 3800
	0    1    -1   0   
$EndComp
$Comp
L +5V #PWR05
U 1 1 58A78949
P 6950 2000
F 0 "#PWR05" H 6950 1850 50  0001 C CNN
F 1 "+5V" V 6950 2200 50  0000 C CNN
F 2 "" H 6950 2000 50  0000 C CNN
F 3 "" H 6950 2000 50  0000 C CNN
	1    6950 2000
	0    1    1    0   
$EndComp
$Comp
L GND #PWR06
U 1 1 58A7AC17
P 6950 2300
F 0 "#PWR06" H 6950 2050 50  0001 C CNN
F 1 "GND" H 6950 2150 50  0000 C CNN
F 2 "" H 6950 2300 50  0000 C CNN
F 3 "" H 6950 2300 50  0000 C CNN
	1    6950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 2850 7250 2850
Wire Wire Line
	7250 2950 7350 2950
Wire Wire Line
	7350 2750 7250 2750
Wire Wire Line
	5450 2350 5250 2350
Wire Wire Line
	5250 2450 5450 2450
Wire Wire Line
	5450 2550 5250 2550
Wire Wire Line
	5250 2150 5350 2150
Wire Wire Line
	7350 3050 7250 3050
Wire Wire Line
	7250 3150 7350 3150
Wire Wire Line
	4650 2650 4650 2550
Wire Wire Line
	4650 2550 4750 2550
Wire Wire Line
	5250 3450 5450 3450
Wire Wire Line
	5450 3550 5250 3550
Wire Wire Line
	5250 3150 5350 3150
Wire Wire Line
	4550 3150 4750 3150
Wire Wire Line
	4750 3250 4550 3250
Wire Wire Line
	4550 3350 4750 3350
Wire Wire Line
	4750 3450 4550 3450
Wire Wire Line
	4650 3650 4650 3550
Wire Wire Line
	4650 3550 4750 3550
Wire Wire Line
	7350 3600 7250 3600
Wire Wire Line
	7250 3700 7350 3700
Wire Wire Line
	7350 3800 7250 3800
Wire Wire Line
	6950 3800 6750 3800
Wire Wire Line
	6950 3700 6750 3700
Wire Wire Line
	6750 3600 6950 3600
Wire Wire Line
	5450 3350 5250 3350
Wire Wire Line
	4550 2450 4750 2450
Wire Wire Line
	4750 2350 4550 2350
Wire Wire Line
	4550 2250 4750 2250
Wire Wire Line
	4750 2150 4550 2150
Text GLabel 7350 3800 2    60   Input ~ 0
MT2_EN
$Comp
L CONN_01X06 P4
U 1 1 58A7901F
P 6550 2900
F 0 "P4" H 6550 3250 50  0000 C CNN
F 1 "CONN_01X06" V 6650 2900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 6550 2900 50  0001 C CNN
F 3 "" H 6550 2900 50  0000 C CNN
	1    6550 2900
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X07 P3
U 1 1 58A790E6
P 6550 2000
F 0 "P3" H 6550 2400 50  0000 C CNN
F 1 "CONN_01X07" V 6650 2000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07" H 6550 2000 50  0001 C CNN
F 3 "" H 6550 2000 50  0000 C CNN
	1    6550 2000
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X08 P5
U 1 1 58A7916F
P 6550 3850
F 0 "P5" H 6550 4300 50  0000 C CNN
F 1 "CONN_01X08" V 6650 3850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 6550 3850 50  0001 C CNN
F 3 "" H 6550 3850 50  0000 C CNN
	1    6550 3850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6750 2000 6950 2000
Wire Wire Line
	6750 2200 6950 2200
Wire Wire Line
	6950 2200 6950 2300
Wire Wire Line
	6750 2100 6850 2100
Wire Wire Line
	6850 2100 6850 2200
Connection ~ 6850 2200
Wire Wire Line
	6750 4000 6950 4000
Wire Wire Line
	7250 4000 7350 4000
$Comp
L R R10
U 1 1 58A82F5B
P 7100 4000
F 0 "R10" V 7150 3850 50  0000 C CNN
F 1 "R" V 7100 4000 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 4000 50  0001 C CNN
F 3 "" H 7100 4000 50  0000 C CNN
	1    7100 4000
	0    1    -1   0   
$EndComp
Wire Wire Line
	6750 3900 6950 3900
Wire Wire Line
	7250 3900 7350 3900
$Comp
L R R9
U 1 1 58A76F8E
P 7100 3900
F 0 "R9" V 7150 3750 50  0000 C CNN
F 1 "R" V 7100 3900 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM7mm" V 7030 3900 50  0001 C CNN
F 3 "" H 7100 3900 50  0000 C CNN
	1    7100 3900
	0    1    -1   0   
$EndComp
Text GLabel 7350 3700 2    60   Input ~ 0
IN2_1B
Text GLabel 7350 3600 2    60   Input ~ 0
IN2_1A
Wire Wire Line
	6750 2750 6950 2750
Wire Wire Line
	6950 2850 6750 2850
Wire Wire Line
	6950 3050 6750 3050
Wire Wire Line
	6750 2950 6950 2950
Wire Wire Line
	6750 3150 6950 3150
$EndSCHEMATC
