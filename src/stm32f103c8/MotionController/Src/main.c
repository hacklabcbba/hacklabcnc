/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "motion.h"
#include "nmethods.h"
#include "neval.h"
#include "cbuffer.h"
#include "math.h"
#include "string.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

typedef double (*func_t)(double x, void *args);
typedef struct
{
	func_t func;
	void *arg;
	double xlim[2];
	double ylim[2];
} segment_t;

void segment_init(segment_t *segment, double (*func)(double, void *), void *arg, double dtime)
{
	segment->func = func;
	segment->arg = arg;
	segment->xlim[0] = 0;
	segment->xlim[1] = dtime;
	segment->ylim[0] = (func)(0, arg);
	segment->ylim[1] = (func)(dtime, arg);
}

typedef void (*solver_f)(double *, double *, double *);
typedef struct
{
	size_t nseg;
	size_t ncoef;
	solver_f solver;
	func_t func;
} system_t;

double fpoly2(double x, void *args)
{
	return neval_polynomial(3, (double*)args, x);
}

double fpoly3(double x, void *args)
{
	return neval_polynomial(4, (double*)args, x);
}

double fpoly5(double x, void *args)
{
	return neval_polynomial(6, (double*)args, x);
}

double fpoly7(double x, void *args)
{
	return neval_polynomial(8, (double*)args, x);
}

typedef struct
{
 uint32_t tticks;
 uint32_t dir;
} step_t;

#define QUEUE_SIZE 100
cbuffer_t queue;
segment_t segment;
system_t system_;
context_t context = {.imax=200, .tol=1.0E-14};
//context_t context = {.imax=200, .tol=1.0E-6};

double coef[28] = {0};
double dtime[7] = {0};
system_t system_p2s3 = {.nseg=3, .ncoef=3, .solver=(solver_f)Motion_Solve_P2S3, .func=fpoly2};
system_t system_p3s7 = {.nseg=7, .ncoef=4, .solver=(solver_f)Motion_Solve_P3S7, .func=fpoly3};
system_t system_p3s1 = {.nseg=1, .ncoef=4, .solver=(solver_f)Motion_Solve_P3S1, .func=fpoly3};
system_t system_p5s1 = {.nseg=1, .ncoef=6, .solver=(solver_f)Motion_Solve_P5S1, .func=fpoly5};
system_t system_p7s1 = {.nseg=1, .ncoef=8, .solver=(solver_f)Motion_Solve_P7S1, .func=fpoly7};
system_t *system = &system_p2s3;

double c[8] = {0.000000000000000, 0.00000000000000, 0.000000000000000, 0.00000000000000, 0.000000000000000, 0.00000000000000, 0.000000000000000, 0.00000000000000};
//double dt7[7] = {0.156250000000000, 0.312500000000000, 0.156250000000000, 1.250000000000000, 0.156250000000000, 0.312500000000000, 0.15625000000000};
//double dt3[3] = {0.625000000000000, 1.250000000000000, 0.62500000000000};
//double dt1[1] = {2.50000000000000};
double dt3[3] = {1, 2, 1};
double dt1[1] = {4};
double *dt = dt3;
int32_t r_end = 250;

int32_t p_cval = 0,
				p_nval = 0,
				p_dval = 0;

double vmax = 500.0,
       amax = 5000.0;

double e, x0;
uint32_t tim_freq;

void step(void);
inline void step(void)
{
	HAL_GPIO_WritePin(MOTOR_STEP_GPIO_Port, MOTOR_STEP_Pin, GPIO_PIN_SET);
  HAL_GPIO_TogglePin(MOTOR_STEP_GPIO_Port, MOTOR_STEP_Pin);
}

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();

  /* USER CODE BEGIN 2 */
	HAL_SuspendTick();

	tim_freq = HAL_RCC_GetHCLKFreq() / htim2.Init.Prescaler;
	queue = cbuffer_create(QUEUE_SIZE, sizeof(step_t));
	while(queue == NULL);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	x0 = 0.0;
	p_cval = 0;
	p_dval = r_end;
	while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
		if(p_cval != p_dval)
		{
			e = 0.0;
			c[0] = p_cval;
			c[1] = p_dval;
			int32_t p_diff = p_dval - p_cval;
			int32_t s_next = (p_diff > 0)? 1: -1;
			memcpy(dtime, dt, sizeof(double)*system->nseg);
			system->solver(coef, dt, c);
			for(size_t i=0; i<system->nseg; i++)
			{
				if(dtime[i] > 0.0)
				{
					segment_init(&segment, system->func, ((double(*)[system->ncoef])coef)[i], dtime[i]);
					while(1)
					{
						p_nval = p_cval + s_next;
						int32_t ylim[2] = {(int32_t)((s_next == 1)? floor(segment.ylim[0]): ceil(segment.ylim[0])), (int32_t)((s_next == 1)? floor(segment.ylim[1]): ceil(segment.ylim[1]))};
						int32_t yldiff[2] = {p_nval - ylim[0], p_nval - ylim[1]};
						uint32_t cnd = (yldiff[0]*yldiff[1] <= 1);
						if(cnd)
						{
							HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
							nmeth_regfalsi(segment.func, segment.arg, p_nval, segment.xlim[0], segment.xlim[1], &context);
              HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);


							uint32_t tticks = (uint64_t)((x0 + context.x) * tim_freq);
							step_t nstep = {.dir = s_next, .tticks = tticks};
							while (!cbuffer_push(queue, &nstep));
							if(__HAL_TIM_GET_IT_SOURCE(&htim2, TIM_IT_CC1) == RESET)
							{
								if(cbuffer_pop(queue, &nstep))
								{
									htim2.Instance->CCR1 = nstep.tticks;
									HAL_GPIO_WritePin(MOTOR_DIR_GPIO_Port, MOTOR_DIR_Pin, (nstep.dir == 1)? GPIO_PIN_SET: GPIO_PIN_RESET);
									htim2.Instance->CNT = 0;
									x0 = 0.0;
									HAL_TIM_OC_Start_IT(&htim2, TIM_CHANNEL_1);
								}
							}
							double y = (segment.func)(context.x, segment.arg);
							e += fabs(y - p_nval);
							p_cval = p_nval;
						}
						else
						{
							break;
						}
					}
					x0 += dtime[i];	
				}
			}
			/* Test */
			p_dval = -p_dval;
		}
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void TIM2_IRQHandler(void)
{
  if (__HAL_TIM_GET_FLAG(&htim2, TIM_FLAG_CC1))
  {
    if (__HAL_TIM_GET_ITSTATUS(&htim2, TIM_IT_CC1))
    {
      __HAL_TIM_CLEAR_IT(&htim2, TIM_FLAG_CC1);
			step();
			step_t nstep;
      if(cbuffer_pop(queue, &nstep))
			{
				htim2.Instance->CCR1 = nstep.tticks;
				HAL_GPIO_WritePin(MOTOR_DIR_GPIO_Port, MOTOR_DIR_Pin, (nstep.dir == 1)? GPIO_PIN_SET: GPIO_PIN_RESET);
			}
			else
			{
				HAL_TIM_OC_Stop_IT(&htim2, TIM_CHANNEL_1);
			}
    }
  }
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
