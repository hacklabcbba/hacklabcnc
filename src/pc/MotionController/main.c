#include <stdio.h>

#include <math.h>
#include <string.h>

#include "motion.h"
#include "nmethods.h"
#include "neval.h"
#include "cbuffer.h"
#include "slink.h"

typedef double (*func_t)(double x, void *args);
typedef struct
{
	func_t func;
	void *arg;
	double xlim[2];
	double ylim[2];
} segment_t;

void segment_init(segment_t *segment, double (*func)(double, void *), void *arg, double dtime)
{
	segment->func = func;
	segment->arg = arg;
	segment->xlim[0] = 0;
	segment->xlim[1] = dtime;
	segment->ylim[0] = (func)(0, arg);
	segment->ylim[1] = (func)(dtime, arg);
}

typedef void (*solver_f)(double *, double *, double *);
typedef struct
{
	size_t nseg;
	size_t ncoef;
	solver_f solver;
	func_t func;
} system_t;

double fpoly1(double x, void *args)
{
	return neval_polynomial(2, (double*)args, x);
}

double fpoly2(double x, void *args)
{
	return neval_polynomial(3, (double*)args, x);
}

double fpoly3(double x, void *args)
{
	return neval_polynomial(4, (double*)args, x);
}

double fpoly4(double x, void *args)
{
	return neval_polynomial(5, (double*)args, x);
}

double fpoly5(double x, void *args)
{
	return neval_polynomial(6, (double*)args, x);
}

double fpoly6(double x, void *args)
{
	return neval_polynomial(7, (double*)args, x);
}

double fpoly7(double x, void *args)
{
	return neval_polynomial(8, (double*)args, x);
}

typedef struct
{
	uint32_t tticks;
	uint32_t dir;
} step_t;

segment_t segment;
system_t system_;
context_t context = {.imax=200, .tol=1.0E-14};

double coef[28] = {0};
double dtime[7] = {0};
system_t system_p2s3 = {.nseg=3, .ncoef=3, .solver=(solver_f)Motion_Solve_P2S3, .func=fpoly2};
system_t system_p3s7 = {.nseg=7, .ncoef=4, .solver=(solver_f)Motion_Solve_P3S7, .func=fpoly3};
system_t system_p4s3 = {.nseg=3, .ncoef=5, .solver=(solver_f)Motion_Solve_P4S3, .func=fpoly4};
system_t system_p6s3 = {.nseg=3, .ncoef=7, .solver=(solver_f)Motion_Solve_P6S3, .func=fpoly6};
system_t system_p1s1 = {.nseg=1, .ncoef=2, .solver=(solver_f)Motion_Solve_P1S1, .func=fpoly1};
system_t system_p3s1 = {.nseg=1, .ncoef=4, .solver=(solver_f)Motion_Solve_P3S1, .func=fpoly3};
system_t system_p5s1 = {.nseg=1, .ncoef=6, .solver=(solver_f)Motion_Solve_P5S1, .func=fpoly5};
system_t system_p7s1 = {.nseg=1, .ncoef=8, .solver=(solver_f)Motion_Solve_P7S1, .func=fpoly7};

double c[8] = {0.000000000000000, 0.00000000000000, 0.000000000000000, 0.00000000000000, 0.000000000000000, 0.00000000000000, 0.000000000000000, 0.00000000000000};
double dt7[7] = {0.25, 0.5, 0.25, 2, 0.25, 0.5, 0.25};
double dt3[3] = {1, 2, 1};
double dt1[1] = {4};
double *dt = dt3;
system_t *system = &system_p6s3;
int64_t r_end = 1000000;

int64_t p_cval = 0,
        p_nval = 0,
        p_dval = 0;

double etot, x0;

int main(void)
{
	slink_message_t message_tx = SLink_Create(SLINK_MAX_PAYLOAD_SIZE);
	slink_message_t message_rx = SLink_Create(SLINK_MAX_PAYLOAD_SIZE);
	SLink_InitMessage(message_tx);
	SLink_InitMessage(message_rx);

	x0 = 0.0;
	p_cval = 0;
	p_dval = r_end;

	etot = 0.0;
	c[0] = p_cval;
	c[1] = p_dval;
	int64_t p_diff = p_dval - p_cval;
	int64_t s_next = (p_diff > 0)? 1: -1;
	memcpy(dtime, dt, sizeof(double)*system->nseg);
	system->solver(coef, dt, c);
	for(size_t i=0; i<system->nseg; i++)
	{
		if(dtime[i] > 0.0)
		{
			segment_init(&segment, system->func, ((double(*)[system->ncoef])coef)[i], dtime[i]);
			while(1)
			{
				p_nval = p_cval + s_next;
				int64_t ylim[2] = {(int64_t)((s_next == 1)? floor(segment.ylim[0]): ceil(segment.ylim[0])), (int64_t)((s_next == 1)? floor(segment.ylim[1]): ceil(segment.ylim[1]))};
				int64_t yldiff[2] = {p_nval - ylim[0], p_nval - ylim[1]};
				uint32_t cnd = (yldiff[0]*yldiff[1] <= 1);
				if(cnd)
				{
					nmeth_regfalsi(segment.func, segment.arg, p_nval, segment.xlim[0], segment.xlim[1], &context);
					double y = (segment.func)(context.x, segment.arg);
					double e = fabs(y - p_nval);
					printf("Time: %12.8f Position: %12.4f Error: %12.8f\n", x0 + context.x, y, e);
					etot += e;
					p_cval = p_nval;
				}
				else
				{
					break;
				}
			}
			x0 += dtime[i];
		}
	}
}
